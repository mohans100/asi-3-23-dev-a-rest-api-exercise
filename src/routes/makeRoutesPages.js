import dayjs from "dayjs"
import PagesModel from "../db/models/PagesModel.js"
import { NotFoundError } from "../errors.js"
import auth from "../middlewares/auth.js"
import mw from "../middlewares/mw.js"
import permission from "../middlewares/permission.js"
import validate from "../middlewares/validate.js"
import { sanitizePage } from "../sanitizers.js"
import { idValidator, statusValidator } from "../validators.js"
const makeRoutesPages = ({ app, db }) => {
  const checkIfPageExists = async (pageId) => {
    const page = await PagesModel.query().findById(pageId)

    if (page) {
      return page
    }

    throw new NotFoundError()
  }
  app.get(
    "/pages",
    mw(async (req, res) => {
      let pages

      if (req.headers.authorization == null) {
        pages = await PagesModel.query()
      } else {
        pages = await PagesModel.query().where("status", "published")
      }

      if (!pages) {
        return
      }

      res.send({ result: pages })
    })
  )
  app.patch(
    "/pages/:id",
    validate({
      params: { id: idValidator.required() },
    }),
    mw(async (req, res) => {
      const {
        data: {
          body: { title, content, urlSlug, status },
          params: { id },
        },
      } = req
      const page = await checkIfPageExists(id, res)

      if (!page) {
        return
      }

      const updatedPage = await PagesModel.query().updateAndFetchById(id, {
        ...(title ? { title } : {}),
        ...(content ? { content } : {}),
        ...(urlSlug ? { urlSlug } : {}),
        ...(status ? { status } : {}),
      })

      res.send({ result: sanitizePage(updatedPage) })
    })
  )
  app.post(
    "/pages",
    auth,
    permission,
    validate({
      body: {
        status: statusValidator.required(),
      },
    }),
    mw(async (req, res) => {
      const { title, content, urlSlug, status } = req.data.body
      const idUserCreator = req.session.user.id
      const publishedDate = dayjs().format("MM/DD/YYYY h:mm A")
      const [page] = await db("pages")
        .insert({
          title,
          content,
          urlSlug,
          status,
          idUserCreator,
          publishedDate,
        })
        .returning("*")
      res.send({ result: sanitizePage(page) })
    })
  )

  app.delete(
    "/pages/:pageId",
    validate({
      params: { pageId: idValidator.required() },
    }),
    auth,
    permission,
    mw(async (req, res) => {
      const { pageId } = req.data.params
      const page = await checkIfPageExists(pageId, res)

      if (!page) {
        return
      }

      await PagesModel.query().deleteById(pageId)
      res.send({ result: sanitizePage(page) })
    })
  )
}

export default makeRoutesPages
