import BaseModel from "./BaseModel.js"

class NavigationModel extends BaseModel {
  static get tableName() {
    return "navigation"
  }
}

export default NavigationModel
