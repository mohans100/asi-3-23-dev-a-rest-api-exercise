export const up = async (knex) => {
  await knex.schema.createTable("role", (table) => {
    table.increments("id")
    table.text("name").notNullable()
    table.text("permissions").notNullable()
  })
}

export const down = async (knex) => {
  await knex.schema.dropTable("role")
}
