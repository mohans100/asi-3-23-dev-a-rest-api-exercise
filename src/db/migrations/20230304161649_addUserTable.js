export const up = async (knex) => {
  await knex.schema.createTable("users", (table) => {
    table.increments("id")
    table.text("email").notNullable().unique()
    table.text("firstName").notNullable()
    table.text("lastName").notNullable()
    table.integer("role").notNullable().references("id").inTable("role")
  })
}

export const down = async (knex) => {
  await knex.schema.dropTable("users")
}
