export const up = async (knex) => {
  await knex.schema.createTable("pages", (table) => {
    table.increments("id")
    table.text("title").notNullable()
    table.text("content").notNullable()
    table.text("urlSlug").unique()
    table
      .integer("idUserCreator")
      .notNullable()
      .references("id")
      .inTable("users")
    table.integer("idUserModifiedIt").references("id").inTable("users")
    table.date("publishedDate")
    table.string("status").checkIn(["draft", "published"]).notNullable()
  })
}

export const down = async (knex) => {
  await knex.schema.dropTable("pages")
}
