import BaseModel from "./BaseModel.js"
import RoleModel from "./RoleModel.js"

class UserModel extends BaseModel {
  static get tableName() {
    return "users"
  }

  static get relationMappings() {
    return {
      creator: {
        modelClass: RoleModel,
        relation: BaseModel.HasOneRelation,
        join: {
          from: "users.role",
          to: "role.id",
        },
      },
    }
  }
}

export default UserModel
