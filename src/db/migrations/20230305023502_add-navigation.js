export const up = async (knex) => {
  await knex.schema.createTable("navigation", (table) => {
    table.increments("id")
    table.text("name").notNullable()
    table.text("Hierarchical").notNullable()
  })
}

export const down = async (knex) => {
  await knex.schema.dropTable("navigation")
}
