import { InvalidAccessError } from "../errors.js"
import mw from "./mw.js"

const ID_ROLE_EDITOR = 3
const permission = mw(async (req, res, next) => {
  const session = req.session

  if (session.user.permission === ID_ROLE_EDITOR) {
    throw new InvalidAccessError()
  }

  next()
})

export default permission
