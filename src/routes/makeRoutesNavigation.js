import NavigationModel from "../db/models/NavigationModel.js"
import { NotFoundError } from "../errors.js"
import auth from "../middlewares/auth.js"
import mw from "../middlewares/mw.js"
import permission from "../middlewares/permission.js"
import validate from "../middlewares/validate.js"
import { sanitizeNavigation } from "../sanitizers.js"
import { idValidator, nameValidator } from "../validators.js"
const makeRoutesNavigations = ({ app, db }) => {
  const checkIfNavigationExists = async (navigationId) => {
    const navigation = await NavigationModel.query().findById(navigationId)

    if (navigation) {
      return navigation
    }

    throw new NotFoundError()
  }
  app.get(
    "/navigations",
    mw(async (req, res) => {
      const navigation = await NavigationModel.query()

      if (!navigation) {
        return
      }

      res.send({ result: navigation })
    })
  )
  app.patch(
    "/navigations/:id",
    validate({
      params: { id: idValidator.required() },
    }),
    mw(async (req, res) => {
      const {
        data: {
          body: { name, Hierarchical },
          params: { id },
        },
      } = req
      const navigation = await checkIfNavigationExists(id, res)

      if (!navigation) {
        return
      }

      const updatedNavigation =
        await NavigationModel.query().updateAndFetchById(id, {
          ...(Hierarchical ? { Hierarchical } : {}),
          ...(name ? { name } : {}),
        })

      res.send({ result: sanitizeNavigation(updatedNavigation) })
    })
  )
  app.post(
    "/navigations",
    auth,
    permission,
    validate({
      body: {
        name: nameValidator.required(),
      },
    }),
    mw(async (req, res) => {
      const { name, Hierarchical } = req.data.body
      const [navigation] = await db("navigation")
        .insert({
          name,
          Hierarchical,
        })
        .returning("*")
      res.send({ result: sanitizeNavigation(navigation) })
    })
  )
  app.delete(
    "/navigations/:navigationId",
    validate({
      params: { navigationId: idValidator.required() },
    }),
    auth,
    permission,
    mw(async (req, res) => {
      const { navigationId } = req.data.params
      const navigation = await checkIfNavigationExists(navigationId, res)

      if (!navigation) {
        return
      }

      await NavigationModel.query().deleteById(navigationId)
      res.send({ result: sanitizeNavigation(navigation) })
    })
  )
}

export default makeRoutesNavigations
