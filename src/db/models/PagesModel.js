import BaseModel from "./BaseModel.js"
import UserModel from "./UserModel.js"

class PagesModel extends BaseModel {
  static get tableName() {
    return "pages"
  }
  static get relationMappings() {
    return {
      creator: {
        modelClass: UserModel,
        relation: BaseModel.HasOneRelation,
        join: {
          from: "pages.idUserCreator",
          to: "user.id",
        },
      },
      modified: {
        modelClass: UserModel,
        relation: BaseModel.HasManyRelation,
        join: {
          from: "pages.idUserModifiedIt",
          to: "user.id",
        },
      },
    }
  }
}

export default PagesModel
