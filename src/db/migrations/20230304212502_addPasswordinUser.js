export const up = async (knex) => {
  await knex.schema.alterTable("users", (table) => {
    table.string("password")
  })
}

export const down = async (knex) => {
  await knex.schema.alterTable("users", (table) => {
    table.dropColumn("password")
  })
}
